#ifndef JOSSIMEVENT_H
#define JOSSIMEVENT_H

#include <QObject>

class jossimEvent : public QObject
{
  Q_OBJECT
public:
  explicit jossimEvent(QObject *parent=Q_NULLPTR):QObject(parent){}

  void go(){emit ready();}
  void go(QString str){emit ready(str);}

signals:
  void ready();
  void ready(QString);
};

#endif // JOSSIMEVENT_H
