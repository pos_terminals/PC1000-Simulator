#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "josinterfase3.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  void updateDisplay();

private slots:
  void on_btnOK_clicked();

  void on_btnUP_clicked();

  void on_btnDOWN_clicked();

  void on_btnNUM1_clicked();

  void on_btnNUM2_clicked();

  void on_btnNUM3_clicked();

  void on_btnNUM4_clicked();

  void on_btnNUM5_clicked();

  void on_btnNUM6_clicked();

  void on_btnNUM7_clicked();

  void on_btnNUM8_clicked();

  void on_btnNUM9_clicked();

  void on_btnNUM0_clicked();

  void on_btnNUM00_clicked();

  void on_btnMENU_clicked();

  void on_btnESC_clicked();

  void on_btnCLR_clicked();

  void on_btnYES_clicked();

private:
  Ui::MainWindow *ui;
  QPixmap DisplayPixmap;
  jossimEvent prnEvent;

  void paintEvent(QPaintEvent*);
  void resizeEvent(QResizeEvent*);
  void keyPressEvent(QKeyEvent *e);

private slots:
  void print(QString str);
};

#endif // MAINWINDOW_H
