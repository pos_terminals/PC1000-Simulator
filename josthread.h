#ifndef JOSTHREAD_H
#define JOSTHREAD_H

#include <QThread>
#include "josinterfase3.h"

namespace POS_PC1000
{
  class JSysThread : public QThread
  {
    Q_OBJECT

  public:
    explicit JSysThread(QObject *parent = 0) : QThread(parent)
    {
      setTerminationEnabled(true);
    }

public slots:
    void stop()
    {
      isRunning = false;
    }

  private:
    bool isRunning;

    void run()
    {
      Lib_LcdCls();

      Lib_LcdPrintxy(8,8,0,"app finished,\n\nOK - restart");
      Lib_KbFlush();

      isRunning = true;
      while (isRunning)
      {
        if (Lib_KbCheck())
        {
          Lib_DelayMs(100);
          continue;
        }
        BYTE ch = Lib_KbGetCh();
        switch(ch)
        {
        case KEYOK:
          emit start_user_thread();
          return;
        }
      }
    }

  signals:
    void start_user_thread();
  };
}
#endif // JOSTHREAD_H
