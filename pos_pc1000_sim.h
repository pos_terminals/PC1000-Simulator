#ifndef POS_PC1000_SIM_H
#define POS_PC1000_SIM_H

#include <QObject>
#include <QSettings>
#include <QApplication>
#include <QMainWindow>

#include "josusrapp.h"

namespace POS_PC1000
{
  class Simulator : public QObject
  {
    Q_OBJECT

  public:
    explicit Simulator(JUserApp * userApp, QObject * parent = 0);
    ~Simulator();

    int initAppAndExec(int argc, char* argv[]);

  private:
    QSettings * env;
    QApplication * a;
    QMainWindow * w;
    QThread * sysTh, * usrTh, * curTh;
    void * pSysTh;
    JUserApp * usrApp;

    void initApp();

  private slots:
    void startSysThread();
    void startUserThread();
    void startCurrentThread();
    void stopCurrentThread();
    void sysThreadFinishHandler();
    void userThreadFinishHandler();
  };
}

#endif // POS_PC1000_SIM_H
