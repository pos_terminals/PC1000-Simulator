#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ui->Printer->clear();

  jossim_setLcd(&DisplayPixmap);
  jossim_setPrintEvent(&prnEvent);

  connect(&prnEvent,SIGNAL(ready(QString)),this,SLOT(print(QString)));
}

MainWindow::~MainWindow()
{
  disconnect(&prnEvent,SIGNAL(ready(QString)),this,SLOT(print(QString)));

  jossim_setLcd(0);
  jossim_setPrintEvent(0);

  delete ui;
}

void MainWindow::updateDisplay()
{
  ui->Display->setPixmap(DisplayPixmap.scaled(ui->Display->width(),ui->Display->height(),Qt::KeepAspectRatio));
}

void MainWindow::on_btnOK_clicked()
{
  jossim_setKbCh(KEYOK);
}

void MainWindow::on_btnUP_clicked()
{
  jossim_setKbCh(KEYUP);
}

void MainWindow::on_btnDOWN_clicked()
{
  jossim_setKbCh(KEYDOWN);
}

void MainWindow::on_btnNUM1_clicked()
{
  jossim_setKbCh(KEY1);
}

void MainWindow::on_btnNUM2_clicked()
{
  jossim_setKbCh(KEY2);
}

void MainWindow::on_btnNUM3_clicked()
{
  jossim_setKbCh(KEY3);
}

void MainWindow::on_btnNUM4_clicked()
{
  jossim_setKbCh(KEY4);
}

void MainWindow::on_btnNUM5_clicked()
{
  jossim_setKbCh(KEY5);
}

void MainWindow::on_btnNUM6_clicked()
{
  jossim_setKbCh(KEY6);
}

void MainWindow::on_btnNUM7_clicked()
{
  jossim_setKbCh(KEY7);
}

void MainWindow::on_btnNUM8_clicked()
{
  jossim_setKbCh(KEY8);
}

void MainWindow::on_btnNUM9_clicked()
{
  jossim_setKbCh(KEY9);
}

void MainWindow::on_btnNUM0_clicked()
{
  jossim_setKbCh(KEY0);
}

void MainWindow::on_btnNUM00_clicked()
{
  jossim_setKbCh(KEY00);
}

void MainWindow::on_btnMENU_clicked()
{
  jossim_setKbCh(KEYMENU);
}

void MainWindow::on_btnESC_clicked()
{
  jossim_setKbCh(KEYCANCEL);
}

void MainWindow::on_btnCLR_clicked()
{
  jossim_setKbCh(KEYCLEAR);
}

void MainWindow::on_btnYES_clicked()
{
  jossim_setKbCh(KEYENTER);
}

void MainWindow::paintEvent(QPaintEvent * event)
{
  updateDisplay();
  QMainWindow::paintEvent(event);
}

void MainWindow::resizeEvent(QResizeEvent * event)
{
  QMainWindow::resizeEvent(event);
  updateDisplay();
}

#include <QKeyEvent>
void MainWindow::keyPressEvent(QKeyEvent* e)
{
  int key = e->key();
  BYTE ch;
  switch (key)
  {
  case Qt::Key_Enter:
    ch = KEYENTER;
    break;

  case Qt::Key_Escape:
    ch = KEYCANCEL;
    break;

  case Qt::Key_Delete:
  case Qt::Key_Backspace:
    ch = KEYCLEAR;
    break;

  case Qt::Key_Menu:
    ch = KEYMENU;
    break;

  case Qt::Key_Home:
    ch = KEYOK;
    break;

  case Qt::Key_PageUp:
    ch = KEYUP;
    break;

  case Qt::Key_PageDown:
    ch = KEYDOWN;
    break;

  default:
    ch = key;
    break;
  }
  jossim_setKbCh(ch);
}

void MainWindow::print(QString str)
{
  ui->Printer->append(str);
}
