#ifndef USERTHREAD_H
#define USERTHREAD_H

#include <QThread>

namespace POS_PC1000
{
  class JUserApp : public QThread
  {
    Q_OBJECT

  public:
    explicit JUserApp(QObject *parent = 0) : QThread(parent)
    {
      setTerminationEnabled(true);
    }

  public slots:
    virtual void stop() = 0;

  private:
    virtual int main() = 0;

    void run()
    {
      int res = main();
      exit(res);
    }
  };
}

#endif // USERTHREAD_H
