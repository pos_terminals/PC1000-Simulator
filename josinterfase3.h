#ifndef JOSINTERFASE3_H
#define JOSINTERFASE3_H

#include <QSettings>
#include <QPixmap>
#include <QString>

#include "jossimEvent.h"

#include "api/3/josapi.h"

void jossim_setEnv(QSettings * env);

void jossim_setLcd(QPixmap * lcd);

void jossim_setKbCh(BYTE ch);

void jossim_setPrintEvent(jossimEvent * event);

#endif // JOSINTERFASE3_H
