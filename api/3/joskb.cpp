#include "josinterfase3.h"
#include "../internal.h"

static BYTE KbBuf[32], KbBufCount;

void init_jossim_kb()
{
  KbBufCount = 0;
  memset(KbBuf, 0, sizeof(KbBuf));
}

void jossim_setKbCh(BYTE ch)
{
  if (KbBufCount >= sizeof(KbBuf))
  {
    qWarning("JOS: Keyboard buf overflow");
    return;
  }
  KbBuf[KbBufCount++] = ch;
}

BYTE  Lib_KbGetCh(void)
{
  while(KbBufCount == 0)
  {
    Lib_DelayMs(100);
  }

  BYTE ch = KbBuf[0];
  --KbBufCount;
  memmove(&KbBuf[0], &KbBuf[1], KbBufCount);
  return ch;
}

BYTE  Lib_KbUnGetCh(void)
{
  EMPTY_FUNC
  return 0;
}

int   Lib_KbCheck(void)
{
  return (KbBufCount ? 0 : KB_NoKey_Err);
}

void  Lib_KbFlush(void)
{
  KbBufCount = 0;
}

int   Lib_KbSound(BYTE mode,WORD DlyTimeMs)
{
  Q_UNUSED(mode)
  Q_UNUSED(DlyTimeMs)
  EMPTY_FUNC
  return 0;
}

int   Lib_KbMute(BYTE mode)
{
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int   Lib_KbGetStr(BYTE *str,BYTE minlen, BYTE maxlen,BYTE mode,WORD timeoutsec)
{
  Q_UNUSED(str)
  Q_UNUSED(minlen)
  Q_UNUSED(maxlen)
  Q_UNUSED(mode)
  Q_UNUSED(timeoutsec)
  EMPTY_FUNC
  return 0;
}

int   Lib_KbGetHzStr(uchar *outstr, uchar max, ushort TimeOut)
{
  Q_UNUSED(outstr)
  Q_UNUSED(max)
  Q_UNUSED(TimeOut)
  EMPTY_FUNC
  return 0;
}
