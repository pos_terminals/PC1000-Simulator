#include "josinterfase3.h"
#include "../internal.h"

#include <QDeadlineTimer>

#include <QFile>
#include <QFileInfo>
#include <QMap>

static void clear_files();

static QMap<uchar,QChar> FontMapRus;
void init_FontMap()
{
  QString rusmap = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя";
  Q_ASSERT(rusmap.count() == 0xff-0xc0+1);
  for (int idx = 0xc0; idx <= 0xff; ++idx)
    FontMapRus[idx] = rusmap[idx-0xc0];
}

QString str_decode_rus(char * str)
{
  int len = strlen(str);
  QString res;

  for (int i = 0; i < len; ++i)
  {
    if((uchar)str[i] >= 0xc0u)
      res += FontMapRus[str[i]];
    else
      res += QChar(str[i]);
  }

  return res;
}

QString str_decode(const char *fmt,va_list argList)
{
  char buf[65536];
  vsnprintf(buf,sizeof(buf),fmt,argList);
  return str_decode_rus(buf);
}

//--------------------------------------------------

int    Lib_ReadLibVer(void)
{
  return 30;
}

//=================================================
//               System functions
//==================================================

extern void init_jossim_kb();
extern void init_jossim_lcd();

static void init_env();
static void init_timers();
int    Lib_AppInit(void)
{
  init_env();
  init_timers();

  init_jossim_kb();
  init_jossim_lcd();

  clear_files();

  init_FontMap();

  return 0;
}

#include <QThread>
void   Lib_DelayMs(int ms)
{
  QThread::msleep(ms);
}

void   Lib_Beep(void)
{
  EMPTY_FUNC
}

void   Lib_Beef(BYTE mode,int DlyTimeMs)
{
  Q_UNUSED(mode)
  Q_UNUSED(DlyTimeMs)
  EMPTY_FUNC
}

int   Lib_SetDateTime(BYTE *datetime)
{
  Q_UNUSED(datetime)
  EMPTY_FUNC
  return 0;
}

int   Lib_GetDateTime(BYTE *datetime)
{
  Q_UNUSED(datetime)
  EMPTY_FUNC
  return 0;
}


#define TIMERS_COUNT 5
static QDeadlineTimer * timers[TIMERS_COUNT];

static void init_timers()
{
  for (int i = 0; i < TIMERS_COUNT; ++i)
    timers[i] = 0;
}

void  Lib_SetTimer(BYTE TimerNo, WORD Cnt100ms)
{
  Q_ASSERT(TimerNo < TIMERS_COUNT);
  if (timers[TimerNo]) delete timers[TimerNo];
  timers[TimerNo] = new QDeadlineTimer(Cnt100ms*100);
}

WORD  Lib_CheckTimer(BYTE TimerNo)
{
  Q_ASSERT(TimerNo < TIMERS_COUNT);
  return timers[TimerNo]->remainingTime();
}

void  Lib_StopTimer(BYTE TimerNo)
{
  Q_ASSERT(TimerNo < TIMERS_COUNT);
  delete timers[TimerNo];
}

//for all hardware and software version
void  Lib_ReadSN(BYTE *SerialNo)
{
  Q_UNUSED(SerialNo)
  EMPTY_FUNC
}

int   Lib_EraseSN(void)
{
  EMPTY_FUNC
  return 0;
}

int   Lib_ReadVerInfo(BYTE *VerInfo)
{
  Q_UNUSED(VerInfo)
  EMPTY_FUNC
  return 0;
}

int		Lib_ReadTerminalType(uchar *posType)
{
  sprintf((char*)posType, POSTYPE_PC1000);
  return 0;
}

void  Lib_Reboot(void)
{
  EMPTY_FUNC
}

void  Lib_GetRand(uchar *out_rand ,uchar  out_len)
{
  Q_UNUSED(out_rand)
  Q_UNUSED(out_len)
  EMPTY_FUNC
}

int		Lib_GetBatChargeStatus(void)
{
  EMPTY_FUNC
  return 0;
}

int   Lib_GetBatteryVolt(void)
{
  EMPTY_FUNC
  return 0;
}

int   Lib_ShareMemWrite(DWORD offset, BYTE *buff, int wlen)
{
  Q_UNUSED(offset)
  Q_UNUSED(buff)
  Q_UNUSED(wlen)
  EMPTY_FUNC
  return 0;
}

int   Lib_ShareMemRead(DWORD offset,BYTE *buff,int rlen)
{
  Q_UNUSED(offset)
  Q_UNUSED(buff)
  Q_UNUSED(rlen)
  EMPTY_FUNC
  return 0;
}

int   Lib_ReadTermID(BYTE  *termID)
{
  Q_UNUSED(termID)
  EMPTY_FUNC
  return 0;
}

int   Lib_WriteTermID(BYTE  *termID)
{
  Q_UNUSED(termID)
  EMPTY_FUNC
  return 0;
}

//========================================
//     Encrypt and decrypt functions
//=========================================

#ifndef JOS_WITHOUT_OPENSSL
#include <openssl/des.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#define DES_FUNC(func,key,bytes,res) \
{ \
  DES_key_schedule Key; \
  int err = DES_set_key((const_DES_cblock*)key, &Key); \
  if (err) \
  { \
    WARN("DES set key error %d", err); \
    return; \
  } \
  DES_ecb_encrypt((const_DES_cblock*)bytes, (DES_cblock *)res, &Key, func); \
  return; \
}
#endif
void  Lib_Des(uchar *bytes, uchar *res,uchar *key, int mode)
{
#ifndef JOS_WITHOUT_OPENSSL
  switch(mode)
  {
  case ENCRYPT:
    DES_FUNC(DES_ENCRYPT,key,bytes,res);
  case DECRYPT:
    DES_FUNC(DES_DECRYPT,key,bytes,res);
  }
#else
  Q_UNUSED(bytes)
  Q_UNUSED(res)
  Q_UNUSED(key)
  Q_UNUSED(mode)
  EMPTY_FUNC
#endif
}

void  Lib_Rsa(uchar* Modul,uint ModulLen,uchar* Exp,uint ExpLen,uchar* DataIn,uchar* DataOut)
{
  Q_UNUSED(Modul)
  Q_UNUSED(ModulLen)
  Q_UNUSED(Exp)
  Q_UNUSED(ExpLen)
  Q_UNUSED(DataIn)
  Q_UNUSED(DataOut)
  EMPTY_FUNC
}

void  Lib_Hash(uchar* DataIn, uint DataInLen, uchar* DataOut)
{
  Q_UNUSED(DataIn)
  Q_UNUSED(DataInLen)
  Q_UNUSED(DataOut)
  EMPTY_FUNC
}

//=============================================
//     Asynchronism communication functions
//
//=============================================

int  Lib_ComReset(BYTE port)
{
  Q_UNUSED(port)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComWrite(BYTE port,BYTE *writebyte,int write_len)
{
  Q_UNUSED(port)
  Q_UNUSED(writebyte)
  Q_UNUSED(write_len)
  EMPTY_FUNC
  return 0;
}
int  Lib_ComRecvByte(BYTE port,BYTE *recv_byte,int waitms)
{
  Q_UNUSED(port)
  Q_UNUSED(recv_byte)
  Q_UNUSED(waitms)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComRecv(BYTE port,BYTE *recv_data,int max_len,int *recv_len,int waitms)
{
  Q_UNUSED(port)
  Q_UNUSED(recv_data)
  Q_UNUSED(max_len)
  Q_UNUSED(recv_len)
  Q_UNUSED(waitms)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComSendByte(BYTE port,BYTE send_byte)
{
  Q_UNUSED(port)
  Q_UNUSED(send_byte)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComSend(BYTE port,BYTE *send_data,int send_len)
{
  Q_UNUSED(port)
  Q_UNUSED(send_data)
  Q_UNUSED(send_len)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComClose(BYTE port)
{
  Q_UNUSED(port)
  EMPTY_FUNC
  return 0;
}

int  Lib_ComOpen(BYTE port, char *ComPara)
{
  Q_UNUSED(port)
  Q_UNUSED(ComPara)
  EMPTY_FUNC
  return 0;
}

//============================================================
//       USB Host and Device Operation Functions
//============================================================

int  Lib_UsbOpen(uchar port)
{
  Q_UNUSED(port)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbClose(uchar port)
{
  Q_UNUSED(port)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbReset(uchar port)
{
  Q_UNUSED(port)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbSend(uchar port,uchar *send_data, int send_len)
{
  Q_UNUSED(port)
  Q_UNUSED(send_data)
  Q_UNUSED(send_len)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbSendByte(uchar port,uchar send_byte)
{
  Q_UNUSED(port)
  Q_UNUSED(send_byte)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbRecv(uchar port,uchar *recv_data,int max_len, int *recv_len,int  waitms)
{
  Q_UNUSED(port)
  Q_UNUSED(recv_data)
  Q_UNUSED(max_len)
  Q_UNUSED(recv_len)
  Q_UNUSED(waitms)
  EMPTY_FUNC
  return 0;
}

int  Lib_UsbRecvByte(uchar port,uchar *recv_byte,int waitms)
{
  Q_UNUSED(port)
  Q_UNUSED(recv_byte)
  Q_UNUSED(waitms)
  EMPTY_FUNC
  return 0;
}

//===========================================
//        Defined for file system
//
//============================================

//File operation functions

static QMap<int,QFile*> files;
int files_idx = 0;

static void clear_files()
{
  foreach(QFile * f, files)
    delete f;
  files.clear();
  files_idx = 0;
}

int  Lib_FileOpen(char *filename, BYTE mode)
{
  Q_UNUSED(mode);

  QFile * f = new QFile(filename);
  int idx = files_idx++;
  files[idx] = f;

  QFile::OpenMode Mode = QFile::ReadWrite | QFile::Text;
  if (!f->open(Mode))
    return FILE_NOT_OPENED;

  return idx;
}

int  Lib_FileRead(int fid, BYTE *dat, int len)
{
  return files[fid]->read((char*)dat,len);
}

int  Lib_FileWrite(int fid, BYTE *dat, int len)
{
  return files[fid]->write((char*)dat,len);
}

int  Lib_FileClose(int fid)
{
  files[fid]->close();
  delete files[fid];
  files.remove(fid);
  return 0;
}

int  Lib_FileSeek(int fid, long offset, BYTE fromwhere)
{
  Q_UNUSED(fid)
  Q_UNUSED(offset)
  Q_UNUSED(fromwhere)
  EMPTY_FUNC
  return 0;
}

long Lib_FileSize(char *filename)
{
  return QFileInfo(filename).size();
}

long Lib_FileFreeSize(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_FileTruncate(int fid,long len)
{
  Q_UNUSED(fid)
  Q_UNUSED(len)
  EMPTY_FUNC
  return 0;
}

int  Lib_FileExist(char *filename)
{
  QFileInfo fI(filename);
  return fI.exists() ? 0 : FILE_NOTEXIST;
}

int  Lib_FileInfo(FILE_INFO* finfo)
{
  Q_UNUSED(finfo)
  EMPTY_FUNC
  return 0;
}

int  Lib_FileExOpen(char *filename, BYTE mode,BYTE* attr)
{
  Q_UNUSED(filename)
  Q_UNUSED(mode)
  Q_UNUSED(attr)
  EMPTY_FUNC
  return 0;
}

int  Lib_FileRemove(const char *filename)
{
  Q_UNUSED(filename)
  EMPTY_FUNC
  return 0;
}

#include <QSettings>
#include <QApplication>
static QSettings * Env = 0;

void jossim_setEnv(QSettings * env)
{
  Env = env;
}

static void init_env()
{
  INFO("Env File: \"%s\"",Env->fileName().toStdString().c_str());
}

#include <string.h>
int  Lib_FileGetEnv(char *name, BYTE *value)
{
  QString key = QString::fromLocal8Bit(name);
  if (!Env->contains(key)) return FILE_NO_MATCH_RECORD;
  strcpy((char*)value, Env->value(key).toString().toLocal8Bit().data());
  return 0;
}

int  Lib_FilePutEnv(char *name, BYTE *value)
{
  Env->setValue(QString::fromLocal8Bit(name),QString::fromLocal8Bit((char*)value));
  return 0;
}

//===================================================
//   Multiple Application Manage Functions
//===================================================

int  Lib_AppReadInfo(BYTE AppNo, APP_MSG* ai)
{
  Q_UNUSED(AppNo)
  Q_UNUSED(ai)
  EMPTY_FUNC
  return 0;
}

int  Lib_AppReadState(BYTE AppNo)
{
  Q_UNUSED(AppNo)
  EMPTY_FUNC
  return 0;
}

int  Lib_AppSetActive(BYTE AppNo, BYTE flag)
{
  Q_UNUSED(AppNo)
  Q_UNUSED(flag)
  EMPTY_FUNC
  return 0;
}

int  Lib_AppRun(BYTE AppNo)
{
  Q_UNUSED(AppNo)
  EMPTY_FUNC
  return 0;
}

//========================================================
//                   PED AND PCI API
//==========================================================

int  Lib_PciWriteMKey(BYTE key_no,BYTE key_len,BYTE *key_data,BYTE mode)
{
  Q_UNUSED(key_no)
  Q_UNUSED(key_len)
  Q_UNUSED(key_data)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciWritePinKey(BYTE key_no,BYTE key_len,BYTE *key_data, BYTE mode, BYTE mkey_no)
{
  Q_UNUSED(key_no)
  Q_UNUSED(key_len)
  Q_UNUSED(key_data)
  Q_UNUSED(mode)
  Q_UNUSED(mkey_no)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciWriteMacKey(BYTE key_no,BYTE key_len,BYTE *key_data, BYTE mode, BYTE mkey_no)
{
  Q_UNUSED(key_no)
  Q_UNUSED(key_len)
  Q_UNUSED(key_data)
  Q_UNUSED(mode)
  Q_UNUSED(mkey_no)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciWriteDesKey(BYTE key_no,BYTE key_len,BYTE *key_data, BYTE mode, BYTE mkey_no)
{
  Q_UNUSED(key_no)
  Q_UNUSED(key_len)
  Q_UNUSED(key_data)
  Q_UNUSED(mode)
  Q_UNUSED(mkey_no)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciDerivePinKey(BYTE mkey_n,BYTE pinkey_n1,BYTE pinkey_n2,BYTE mode)
{
  Q_UNUSED(mkey_n)
  Q_UNUSED(pinkey_n1)
  Q_UNUSED(pinkey_n2)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}
int  Lib_PciDeriveMacKey(BYTE mkey_n,BYTE mackey_n1,BYTE mackey_n2,BYTE mode)
{
  Q_UNUSED(mkey_n)
  Q_UNUSED(mackey_n1)
  Q_UNUSED(mackey_n2)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciDeriveDesKey(BYTE mkey_n,BYTE deskey_n1,BYTE deskey_n2,BYTE mode)
{
  Q_UNUSED(mkey_n)
  Q_UNUSED(deskey_n1)
  Q_UNUSED(deskey_n2)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciGetPin(BYTE pinkey_n,BYTE min_len,BYTE max_len,BYTE *card_no,BYTE mode,BYTE *pin_block,WORD waittime_sec)
{
  Q_UNUSED(pinkey_n)
  Q_UNUSED(min_len)
  Q_UNUSED(max_len)
  Q_UNUSED(card_no)
  Q_UNUSED(mode)
  Q_UNUSED(pin_block)
  Q_UNUSED(waittime_sec)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciGetMac(BYTE mackey_n,WORD inlen,BYTE *indata,BYTE *macout,BYTE mode)
{
  Q_UNUSED(mackey_n)
  Q_UNUSED(inlen)
  Q_UNUSED(indata)
  Q_UNUSED(macout)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciDes(BYTE deskey_n,BYTE *indata,BYTE *outdata,BYTE mode)
{
  Q_UNUSED(deskey_n)
  Q_UNUSED(indata)
  Q_UNUSED(outdata)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciGetRnd (BYTE *rnd8)
{
  Q_UNUSED(rnd8)
  EMPTY_FUNC
  return 0;
}

int  Lib_PciAccessAuth(BYTE *auth_data,BYTE mode)
{
  Q_UNUSED(auth_data)
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

int Lib_PciLoadKeyDukpt(uchar KeyId,uchar BdkLen,uchar KsnLen,uchar *BdkKsn)
{
  Q_UNUSED(KeyId)
  Q_UNUSED(KsnLen)
  Q_UNUSED(BdkLen)
  Q_UNUSED(BdkKsn)
  EMPTY_FUNC
  return 0;
}

int Lib_PciGetPinDukpt(uchar key_n,uchar min_len,uchar max_len,uchar *card_no,uchar mode,uchar *pin_block,ushort waittime_sec,uchar *out_ksn)
{
  Q_UNUSED(key_n)
  Q_UNUSED(min_len)
  Q_UNUSED(max_len)
  Q_UNUSED(card_no)
  Q_UNUSED(mode)
  Q_UNUSED(pin_block)
  Q_UNUSED(waittime_sec)
  Q_UNUSED(out_ksn)
  EMPTY_FUNC
  return 0;
}

int Lib_PciGetMacDukpt(uchar key_n,ushort inlen,uchar *indata,uchar *macout,uchar mode,uchar *out_ksn)
{
  Q_UNUSED(key_n)
  Q_UNUSED(inlen)
  Q_UNUSED(indata)
  Q_UNUSED(macout)
  Q_UNUSED(mode)
  Q_UNUSED(out_ksn)
  EMPTY_FUNC
  return 0;
}

//==================================================================
//    FONT API
//===================================================================

int  Lib_FontFileCheck(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_FontGetCharSet(BYTE *bCharSet, BYTE *bNum)
{
  Q_UNUSED(bCharSet)
  Q_UNUSED(bNum)
  EMPTY_FUNC
  return 0;
}

int  Lib_FontGetHeight(BYTE bCharSet, BYTE *bHeight, BYTE * bHeightNum)
{
  Q_UNUSED(bCharSet)
  Q_UNUSED(bHeight)
  Q_UNUSED(bHeightNum)
  EMPTY_FUNC
  return 0;
}

int Lib_GetFontDotMatrix(BYTE *strIn, BYTE byFontHeight,
                     BYTE *pbyCharDotMatrix, BYTE *pbyOutCharLen)
{
  Q_UNUSED(strIn)
  Q_UNUSED(byFontHeight)
  Q_UNUSED(pbyCharDotMatrix)
  Q_UNUSED(pbyOutCharLen)
  EMPTY_FUNC
  return 0;
}

//========================================================
//                   Wireless comm API
//==========================================================

void  Lib_WlsIoCtl(unsigned char ioname,unsigned char iomode)
{
  Q_UNUSED(ioname)
  Q_UNUSED(iomode)
  EMPTY_FUNC
}

//=============================================
//     PICC  Reader Functions
//=============================================

int  Lib_PiccOpen(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccCheck(uchar mode,uchar *CardType,uchar *SerialNo)
{
  Q_UNUSED(mode)
  Q_UNUSED(CardType)
  Q_UNUSED(SerialNo)
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccCommand(APDU_SEND *ApduSend, APDU_RESP *ApduResp)
{
  Q_UNUSED(ApduSend)
  Q_UNUSED(ApduResp)
  EMPTY_FUNC
  return 0;
}

void Lib_PiccHalt(void)
{
  EMPTY_FUNC
}

int  Lib_PiccRemove(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccReset(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccClose(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccM1Authority(uchar Type,uchar BlkNo,uchar *Pwd,uchar *SerialNo)
{
  Q_UNUSED(Type)
  Q_UNUSED(BlkNo)
  Q_UNUSED(Pwd)
  Q_UNUSED(SerialNo)
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccM1ReadBlock(uchar BlkNo,uchar *BlkValue)
{
  Q_UNUSED(BlkNo)
  Q_UNUSED(BlkValue)
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccM1WriteBlock(uchar BlkNo,uchar *BlkValue)
{
  Q_UNUSED(BlkNo)
  Q_UNUSED(BlkValue)
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccM1Operate(uchar Type,uchar BlkNo,uchar *Value,uchar UpdateBlkNo)
{
  Q_UNUSED(Type)
  Q_UNUSED(BlkNo)
  Q_UNUSED(Value)
  Q_UNUSED(UpdateBlkNo)
  EMPTY_FUNC
  return 0;
}

int  Lib_PiccCommandSRT512(uchar needreceive,uchar *incmd,uchar cmdlen,uchar *rsp,int *rsp_len)
{
  Q_UNUSED(needreceive)
  Q_UNUSED(incmd)
  Q_UNUSED(cmdlen)
  Q_UNUSED(rsp)
  Q_UNUSED(rsp_len)
  EMPTY_FUNC
  return 0;
}

//=========================================================================================
//
// Smart card functions
//
//=========================================================================================

int Lib_IccClose(uchar slot)
{
  Q_UNUSED(slot)
  EMPTY_FUNC
  return 0;
}

int Lib_IccCheck(uchar slot)
{
  Q_UNUSED(slot)
  EMPTY_FUNC
  return 0;
}

int Lib_IccCommand(uchar slot,APDU_SEND * ApduSend,APDU_RESP * ApduResp)
{
  Q_UNUSED(slot)
  Q_UNUSED(ApduSend)
  Q_UNUSED(ApduResp)
  EMPTY_FUNC
  return 0;
}

int Lib_IccOpen(uchar slot,uchar VCC_Mode,uchar *ATR)
{
  Q_UNUSED(slot)
  Q_UNUSED(VCC_Mode)
  Q_UNUSED(ATR)
  EMPTY_FUNC
  return 0;
}

int Lib_IccSetAutoResp(uchar slot,uchar autoresp)
{
  Q_UNUSED(slot)
  Q_UNUSED(autoresp)
  EMPTY_FUNC
  return 0;
}

int Lib_IccPPS(uchar slot,uchar *pps)
{
  Q_UNUSED(slot)
  Q_UNUSED(pps)
  EMPTY_FUNC
  return 0;
}
