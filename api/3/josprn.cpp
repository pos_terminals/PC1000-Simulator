#include "josinterfase3.h"
#include "../internal.h"

static QString Buffer;
static jossimEvent * Event;

void jossim_setPrintEvent(jossimEvent * event)
{
  Event = event;
}

int  Lib_PrnInit(void)
{
  return 0;
}

int  Lib_PrnStr(char *str,...)
{
  QString Str;
  DECODE_FMT_STR(str,Str);
  Buffer.append(Str);
  return 0;
}

int  Lib_PrnLogo(BYTE *logo)
{
  Q_UNUSED(logo)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnStart(void)
{
  if (Event)
    Event->go(Buffer);
  Buffer.clear();
  return 0;
}
int  Lib_PrnCheck(void)
{
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnStep(int pixel)
{
  Q_UNUSED(pixel)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnSetLeftIndent(int x)
{
  Q_UNUSED(x)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnSetSpace(BYTE x, BYTE y)
{
  Q_UNUSED(x)
  Q_UNUSED(y)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnGetFont(BYTE *AsciiFontHeight, BYTE *ExtendFontHeight, BYTE *Zoom)
{
  Q_UNUSED(AsciiFontHeight)
  Q_UNUSED(ExtendFontHeight)
  Q_UNUSED(Zoom)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnSetFont(BYTE AsciiFontHeight, BYTE ExtendFontHeight, BYTE Zoom)
{
  Q_UNUSED(AsciiFontHeight)
  Q_UNUSED(ExtendFontHeight)
  Q_UNUSED(Zoom)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnSetGray(BYTE nLevel)
{
  Q_UNUSED(nLevel)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnSetSpeed(int iSpeed)
{
  Q_UNUSED(iSpeed)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnPaper(uchar mode, WORD pixel)
{
  Q_UNUSED(mode)
  Q_UNUSED(pixel)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnReadVersion(uchar * VersionBuf)
{
  Q_UNUSED(VersionBuf)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnBarcode128(uchar *str,uchar mode,ushort height,uchar width)
{
  Q_UNUSED(str)
  Q_UNUSED(mode)
  Q_UNUSED(height)
  Q_UNUSED(width)
  EMPTY_FUNC
  return 0;
}

int  Lib_PrnBarcodeITF25(uchar *str,ushort height,uchar nZoomIn)
{
  Q_UNUSED(str)
  Q_UNUSED(height)
  Q_UNUSED(nZoomIn)
  EMPTY_FUNC
  return 0;
}
