#include "josinterfase3.h"
#include "../internal.h"

#include <QPixmap>
#include <QPainter>

#include <QMap>

#include <stdio.h>
#include <string.h>

static QPixmap * Lcd = 0;
static QColor ColorBack, Color;
static QPoint Pos;
static QFont Font;
static bool isReverse;
static enum FontMode {font8x6,font16x8} fontMode;

void init_jossim_lcd()
{
  Color = QColor(Qt::black);
  ColorBack = QColor(144,144,144);

  Pos = QPoint(0,0);

  Lib_LcdSetFont(8,16,0);

  isReverse = false;
}
void jossim_setLcd(QPixmap * pixmap)
{
  Lcd = pixmap;
  if (!pixmap) return;

  *Lcd = QPixmap(128,64);
  Lib_LcdSetGray(44);
  Lib_LcdCls();
}

//---------------------------------------------------------

static inline int font_height(QFont F)
{
#if 1
  Q_UNUSED(F)
  switch(fontMode)
  {
  default:
  case font8x6:
    return 8;
  case font16x8:
    return 16;
  }
#else
  return QFontMetrics(F).height();
#endif
}

static inline int font_width(QFont F)
{
#if 1
  Q_UNUSED(F)
  switch(fontMode)
  {
  default:
  case font8x6:
    return 6;
  case font16x8:
    return 8;
  }
#else
  return QFontMetrics(F).averageCharWidth();
#endif
}

static inline BYTE str_width(QFont font, QString str)
{
#if 1
  return str.size() * font_width(font);
#else
  QFontMetrics metr(font);
  return metr.width(str);
#endif
}

static inline int font_yShift(QFont F)
{
  QFontMetrics metr(F);
  return metr.ascent();
}

static inline void set_font_height(QFont & F, BYTE h)
{
  switch (h)
  {
  default:
  case 8:
    fontMode = font8x6;
    F.setPixelSize(8);
    break;

  case 16:
    fontMode = font16x8;
    F.setPixelSize(12);
    break;
  }

#if 0
  QFontMetrics Metrics(F);
  int stretch = 100, width = Metrics.averageCharWidth();
  switch (h)
  {
  default:
  case 8:
    stretch = 100*6/width;
    break;

  case 16:
    stretch = 100*8/width;
    break;
  }
  F.setStretch(stretch);
#endif
}

#define INTR_DRAW_STR_LF \
do{ \
  x = 0; \
  y += h; \
  }while(0)

static void draw_str(int x, int y, QPainter & Painter, QString str)
{  
  int h = font_height(Painter.font()), w = font_width(Painter.font()),
      hShift = font_yShift(Painter.font());

  for (int i = 0; i < str.count(); ++i)
  {
    if (str[i] == '\n')
    {
      INTR_DRAW_STR_LF;
      continue;
    }
    bool isXshift = true;
    if (x+w > Painter.viewport().width())
    {
      isXshift = false;
      INTR_DRAW_STR_LF;
    }
    if (y > Painter.viewport().height() - h)
      y = 0;
    Painter.fillRect(QRect(x,y,w,h),Painter.background());
    Painter.drawText(QPoint(x,y+hShift), QString(str[i]));
    if (isXshift) x += w;
  }
  Pos.setX(x); Pos.setY(y);
}

#define INIT_PAINTER_BASE \
  QPainter Painter; \
  Painter.begin(Lcd);

#define CLOSE_PAINTER \
  Painter.end();

#define INIT_PAINTER_ALL(ColorBack, Color, Font, isReverse) \
  INIT_PAINTER_BASE \
  Painter.setBackground(QBrush(isReverse ? Color : ColorBack)); \
  Painter.setPen(isReverse ? ColorBack : Color); \
  Painter.setFont(Font); \
  Painter.setRenderHint(QPainter::TextAntialiasing, false);

#define INIT_PAINTER INIT_PAINTER_ALL(ColorBack, Color, Font, isReverse)

#define DRAW_STR(x,y,str) \
  draw_str(x,y,Painter,str)

void Lib_LcdCls(void)
{
  if (!Lcd) return;
  INIT_PAINTER
  Painter.fillRect(Painter.viewport(), ColorBack);
  CLOSE_PAINTER
}

void Lib_LcdClrLine(BYTE startline, BYTE endline)
{
  if (!Lcd) return;
  INIT_PAINTER
  Painter.fillRect(0,startline,
                   Painter.viewport().width(),endline-startline,
                   ColorBack);
  CLOSE_PAINTER
}

void Lib_LcdDrawLogo(BYTE *pDataBuffer)
{
  if (!Lcd) return;
  Q_UNUSED(pDataBuffer)
  EMPTY_FUNC
}

void Lib_LcdDrawBox(BYTE x1,BYTE y1,BYTE x2,BYTE y2)
{
  if (!Lcd) return;
  INIT_PAINTER
  Painter.drawRect(x1,y1, x2-x1, y2-y1);
  CLOSE_PAINTER
}

void Lib_LcdDrawLine (BYTE x1,BYTE y1,BYTE x2,BYTE y2, BYTE byColor)
{
  if (!Lcd) return;
  QColor c = byColor ? Color : ColorBack;
  INIT_PAINTER_ALL(ColorBack, c, Font, isReverse)
  Painter.drawLine(x1,y1,x2,y2);
  CLOSE_PAINTER
}

int  Lib_Lcdprintf(const char *fmt,...)
{
  if (!Lcd) return 0;

  QString str;
  DECODE_FMT_STR(fmt,str);

  INIT_PAINTER
  DRAW_STR(Pos.x(),Pos.y(), str);
  CLOSE_PAINTER
  return 0;
}

void Lib_LcdPrintxy(BYTE col,BYTE row,BYTE mode,const char *fmt,...)
{
  if (!Lcd) return;

  QString str;
  DECODE_FMT_STR(fmt,str);

  QFont f = Font;
  if (mode & 0x01)
    set_font_height(f,16);
  else
    set_font_height(f,8);
  bool isRev = (mode & 0x80) == 0x80;
  INIT_PAINTER_ALL(ColorBack,Color,f,isRev)
  DRAW_STR(col,row,str);
  CLOSE_PAINTER
}

void Lib_SprintfFloat(char *strOut, float fNumber, int iDecFractionLen)
{
  if (!Lcd) return;
  Q_UNUSED(strOut)
  Q_UNUSED(fNumber)
  Q_UNUSED(iDecFractionLen)
  EMPTY_FUNC
}

void Lib_LcdPrintFloat(float fNumber, int iDecFractionLen)
{
  if (!Lcd) return;
  Q_UNUSED(fNumber)
  Q_UNUSED(iDecFractionLen)
  EMPTY_FUNC
}

void Lib_LcdDrawPlot (BYTE XO,BYTE YO,BYTE Color)
{
  if (!Lcd) return;
  Q_UNUSED(XO)
  Q_UNUSED(YO)
  Q_UNUSED(Color)
  EMPTY_FUNC
}

void Lib_LcdSetBackLight(BYTE mode)
{
  if (!Lcd) return;
  Q_UNUSED(mode)
  EMPTY_FUNC
}

void Lib_LcdSetGray(BYTE level)
{
  Q_ASSERT(level >= 15);
  Q_ASSERT(level <= 58);

  level += 100;
  ColorBack = QColor(level,level,level);
}

void Lib_LcdGotoxy(BYTE x, BYTE y)
{
  Pos = QPoint(x,y);
}

int  Lib_LcdSetFont(BYTE AsciiFontHeight, BYTE ExtendFontHeight, BYTE Zoom)
{
  Q_UNUSED(Zoom)
  Q_UNUSED(ExtendFontHeight)

  Font = QFont("Lucida Console");
  Font.setStyleHint(QFont::Monospace);
  set_font_height(Font,AsciiFontHeight);
  return 0;
}

int  Lib_LcdGetFont(BYTE *AsciiFontHeight, BYTE *ExtendFontHeight, BYTE *Zoom)
{
  *AsciiFontHeight = Font.pixelSize();
  *ExtendFontHeight = -1;
  *Zoom = -1;
  return 0;
}

BYTE Lib_LcdSetAttr(BYTE attr)
{
  BYTE old = isReverse;
  isReverse = attr != 0;
  return old;
}

int  Lib_LcdRestore(BYTE mode)
{
  if (!Lcd) return 0;
  Q_UNUSED(mode)
  EMPTY_FUNC
  return 0;
}

void Lib_LcdGetSize(BYTE * x, BYTE *y)
{
  *x = Lcd->width();
  *y = Lcd->height();
}

BYTE Lib_LcdGetSelectItem(BYTE *pbyItemString, BYTE byItemNumber, BYTE byExitMode)
{
  Q_UNUSED(pbyItemString)
  Q_UNUSED(byItemNumber)
  Q_UNUSED(byExitMode)
  EMPTY_FUNC
  return 0;
}

BYTE Lib_LcdGetSelectItemCE(BYTE *pbyChnItemString, BYTE *pbyEngItemString,
                     BYTE byItemNumber, BYTE byExitMode)
{
  Q_UNUSED(pbyChnItemString)
  Q_UNUSED(pbyEngItemString)
  Q_UNUSED(byItemNumber)
  Q_UNUSED(byExitMode)
  EMPTY_FUNC
  return 0;
}

void Lib_LcdPrintfCE(char *pCHN , char *pEN)
{
  if (!Lcd) return;
  Q_UNUSED(pCHN)
  Q_UNUSED(pEN)
  EMPTY_FUNC
}

void Lib_LcdPrintxyCE(BYTE col, BYTE row, BYTE mode, char *pCHN , char *pEN)
{
  if (!Lcd) return;
  Q_UNUSED(col)
  Q_UNUSED(row)
  Q_UNUSED(mode)
  Q_UNUSED(pCHN)
  Q_UNUSED(pEN)
  EMPTY_FUNC
}

void Lib_LcdPrintxyExtCE(BYTE col, BYTE row, BYTE mode, char * pCHN, char * pEN, int iPara)
{
  if (!Lcd) return;
  Q_UNUSED(col)
  Q_UNUSED(row)
  Q_UNUSED(mode)
  Q_UNUSED(pCHN)
  Q_UNUSED(pEN)
  Q_UNUSED(iPara)
  EMPTY_FUNC
}

void Lib_LcdGetxy(BYTE *x, BYTE *y)
{
  *x = Pos.x();
  *y = Pos.y();
}

void Lib_LcdSetDotBuf(BYTE *dotbuf)
{
  Q_UNUSED(dotbuf)
  EMPTY_FUNC
}

void Lib_LcdGetDotBuf(BYTE *dotbuf)
{
  Q_UNUSED(dotbuf)
  EMPTY_FUNC
}

void Lib_LcdClrDotBuf(void)
{
  EMPTY_FUNC
}

void Lib_LcdDispDotBuf(void)
{
  if (!Lcd) return;
  EMPTY_FUNC
}
