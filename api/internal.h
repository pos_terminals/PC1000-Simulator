#ifndef INTERNAL_H
#define INTERNAL_H

#include <QString>

#define EMPTY_FUNC \
  qWarning("%s no impementation", __func__);

#define INFO(x,...) qInfo("%s: " x, __func__, ##__VA_ARGS__)
#define WARN(x,...) qWarning("%s: " x, __func__, ##__VA_ARGS__)

QString str_decode_rus(char * str);
QString str_decode(const char *fmt,va_list argList);
#define DECODE_FMT_STR(fmt, str) \
do{ \
  va_list argList; \
  va_start(argList,fmt); \
  str = str_decode(fmt,argList); \
  va_end(argList); \
}while(0)

#endif // INTERNAL_H
